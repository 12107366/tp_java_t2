package org.e12107366.serie6.exo15;
import java.util.Set;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
public class Exo15 {
	public static void main(String[] args) {

		ActorsAndMovies actorsAndMovies = new ActorsAndMovies();
		Set<Movie> movies = actorsAndMovies.readMovies();
		Set<Movie> actors = actorsAndMovies.readMovies();
		// 1
		long nbrFilms = movies.stream().count();
		System.out.println("le nombre de films est :	"+nbrFilms);
		// 2
		long nbrActeurs = movies.stream().flatMap(movie -> movie.actors().stream()).distinct().count();
		System.out.println("le nombre d'acteurs est  :" +nbrActeurs);
		//3
		long nbrAnnees = movies.stream().map(movie -> movie.releaseYear()).count();
		System.out.println("le nombre d'années  :" +nbrAnnees);
		//4
		long plusAncien = movies.stream().min(Comparator.comparing(movie -> movie.releaseYear())).get().releaseYear();
		System.out.println("le plus anciens est  :" +plusAncien);
		long plusRecent = movies.stream().max(Comparator.comparing(movie -> movie.releaseYear())).get().releaseYear();
		System.out.println("le plus recent est  :" +plusRecent);
		//5
		Map<Integer, Long> filmParAnnee = movies.stream().collect(Collectors.groupingBy(movie -> movie.releaseYear(), Collectors.counting()));
		Optional<Entry<Integer, Long>> anneePlusFilm = filmParAnnee.entrySet().stream().max(Map.Entry.comparingByValue());
		if (anneePlusFilm.isPresent()) {
			System.out.println("Année avec le plus de films : " + anneePlusFilm.get().getKey());
			System.out.println("Nombre de films : " + anneePlusFilm.get().getValue());
		}
		//6
		Optional<Movie>filmPlusActeurs = movies.stream().max(Comparator.comparingInt(movie -> movie.actors().size()));
		if (filmPlusActeurs.isPresent()) {
			System.out.println("plus grd nbr dacteurs: " +filmPlusActeurs.get().title());
			System.out.println("nbr dacreur : " +filmPlusActeurs.get().actors().size());
		}

		// qst 7
		Map<Actor, Long> actorMovieCounts = movies.stream().flatMap(movie ->
		movie.actors().stream()).collect(Collectors.groupingBy(actor -> actor, Collectors.counting()));
		Optional<Map.Entry<Actor, Long>> acteurPlusFilms = actorMovieCounts.entrySet().stream()
				.max(Map.Entry.comparingByValue());

		if (acteurPlusFilms.isPresent()) {
			System.out.println("Acteurs avec le plus de films " + acteurPlusFilms.get()
			.getKey().firstName()+" "+acteurPlusFilms.get().getKey().lastName());
			System.out.println("nombre de films	" + acteurPlusFilms.get().getValue());
		}


		//A
		Comparator<Actor> actorComparator = Comparator.comparing(Actor::LastName).thenComparing(Actor::FirstName);
		
		/*	BiFunction<stream<Actor>, Actor, stream<Map.Entry<Actor, Actor>>> pairesAvecActeurs = (actors, actor) ->
	    actors.map(other -> Map.entry(other, actor));

	   

	    Function<Movie, Stream<Actor>> ActeursDansFilm = movie ->
	    movie.getActor.steam(); */
		//d
		BiFunction<Movie, Actor, Stream<Map.Entry<Actor, Actor>>> pairActeurs = (movie, actor) ->
		actorsInMovie.apply(movie).flatMap(other -> pairActeurs.apply(Stream.of(other), actor));
		//e
		Function<Movie, Stream<Map.Entry<Actor, Actor>>> pairesActeursFilm = movie ->
		actorsInMovie.apply(movie).flatMap(actor -> pairActeurs.apply(movie, actor));

		//f
		long nbrPaires = movies.stream().flatMap(pairesActeursFilm).count();
		long pairesUnique = movies.stream().flatMap(pairesActeursFilm).distinct().count();
	}
}