package org.e12107366.serie4.exo09;

import java.util.function.Predicate;

public class Exercice9 {

	public  static void main(String[] args)
	{ 
		//Predicate<TypeduConsummer> nomduconssumer= (type    nomdevariable) -> ce quiil faut faire sur s
		//predicate renvoie boolean
		// nomConssumer.test(arg)
		String a="";
		String b="Jinco";


		Predicate <String> h,i,P,q,r;
		P= (String s)-> s.length()>4;
		System.out.println("la chaine est sup à 4"+P.test("bonjour"));
		q=(String s)->!s.isEmpty();
		System.out.println(q.test(a));
		h=(String s)->s.startsWith("J");
		System.out.println(h.test("JSK"));
		i=(String s)->s.length()==5;
		System.out.println(i.test("cinqo"));
		r=(String s)->s.startsWith("J") && s.length()==5;
		System.out.println(r.test("Jinqo"));
	}
}
