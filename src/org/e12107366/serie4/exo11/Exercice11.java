package org.e12107366.serie4.exo11;
import java.util.Comparator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
public class Exercice11 {
	public static void main(String[] args) {

		Comparator<String> comparator = (s1,s2) -> s1.length()- s2.length();
		System.out.println(comparator.compare("BONJOUR", "boujour"));

		Person p1 = new Person("Gharras", "Hedjoubi", 24);
		Person p2 = new Person("Ihdene", "Lysa", 24);
		
	
		
		Comparator<Person> comparatorLastName= (person1,person2) -> person1.getLastName().length()- person2.getLastName().length();
		System.out.println(comparatorLastName.compare(p1, p2));

		Comparator<Person> comparator2= (Person a,Person b) ->  
		a.getLastName().length()-b.getLastName().length()==0? a.getFirstName().length()-b.getFirstName().length():a.getLastName().length()-b.getLastName().length();
		System.out.println( comparator2.compare(p1,p2));

		
		Comparator<Person> comparator3= (Person a,Person b) ->  
		a.getFirstName().length()-b.getFirstName().length()==0?a.getLastName().length()-b.getLastName().length():a.getFirstName().length()-b.getFirstName().length();
		System.out.println( comparator3.compare(p1,p2));
		
		//QST 5
		
		
		List<Person> ListeDePerson = Arrays.asList(null, null, p2, null,p1 ); 	
		Comparator<Person> comparatorSorted = Comparator.nullsLast(comparatorLastName);  
		ListeDePerson.sort(comparatorSorted);
		System.out.println(ListeDePerson);

		

	}
}



