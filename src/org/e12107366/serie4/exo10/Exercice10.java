package org.e12107366.serie4.exo10;

import java.util.function.BiFunction;
import java.util.function.Function;

public class Exercice10 {
	
	
	
	public static void main(String[] args) {
		//Predicate<TypeduConsummer> nomduconssumer= (type    nomdevariable) -> ce quiil faut faire sur s
		//qst1
		Function <String,String> majuscule=s->s.toUpperCase();
		System.out.println(majuscule.apply("bonjour")); 

		//qst2
		Function <String,String> same=s->
		{if(s.isEmpty())
			return "";
		else 
			return s; 

		};

		//qst3
		Function <String,Integer> taille=s->
		{if(s.isEmpty())
			return 0;
		else 
			return s.length(); 

		};
		System.out.println(taille.apply("")); 



		//qst4
		Function <String,String> parenthese=s->
		{if(s.isEmpty())
			return "()";
		else 
			return "("+s+")"; 

		};

		System.out.println(parenthese.apply("bonjour")); 


		//qst 5
		BiFunction <String,String,Integer> position = (s1,s2)->s1.indexOf(s2);  
		System.out.println(position.apply("bonjour","w"));

		//qst6
		Function <String,Integer> position2 = s2-> "abcdefghi".indexOf(s2);
		System.out.println(position2.apply("c"));
	}

	
	

}
