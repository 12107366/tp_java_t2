package org.e12107366.serie1.exo02;

public class Main {

	public static void main(String[] args) {
		Bissextile annee = new Bissextile() ;
		System.out.println("L'annee 1900 est une année bissextile:" +annee.bissextile(1900)) ;
		System.out.println("L'annee 1901 est une année bissextile: " +annee.bissextile(1901)) ;
		System.out.println("L'annee 1996 est une année bissextile: " +annee.bissextile(1996)) ;
		System.out.println("L'annee 2000 est une année bissextile: " +annee.bissextile(2000)) ;
	}

}

