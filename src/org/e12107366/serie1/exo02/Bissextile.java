package org.e12107366.serie1.exo02;

public class Bissextile {
	public boolean bissextile(int annee) {
		if(annee% 4==0 &&annee%100!=0)
			return true;
		else if(annee % 400==0)
			return true;
		else
			return false;
	}
}
