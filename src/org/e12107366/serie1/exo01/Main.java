package org.e12107366.serie1.exo01;

public class Main {
	public static void main(String[] args) {
		
		Factorielle fact = new Factorielle() ;
		System.out.println("Méthode int Factorielle de 10 = " + fact.intFactorielle(10));
		System.out.println("Méthode double Factorielle de 10 = " +fact.doubleFactorielle(10));
		System.out.println("Méthode BigInteger Factorielle de 10 = " +fact.bigIntFactorielle(10)+"\n");
		
		System.out.println("Méthode int Factorielle de 13 = " + fact.intFactorielle(13));
		System.out.println("Méthode double Factorielle de 13 = " +fact.doubleFactorielle(13));
		System.out.println("Méthode BigInteger Factorielle de 13 = " +fact.bigIntFactorielle(13)+"\n");

		
		System.out.println(" Méthode int Factorielle de 25 = " + fact.intFactorielle(25));
		System.out.println("Méthode double Factorielle de 25 = " +fact.doubleFactorielle(25));
		System.out.println("Méthode BigInteger Factorielle de 25 = " +fact.bigIntFactorielle(25));

		/*La méthode Biginteger donne des resultats plus précis */

	}

}
