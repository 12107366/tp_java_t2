package org.e12107366.serie1.exo01;
import java.math.BigInteger;

public class Factorielle{

	public int intFactorielle (int nombre)
	{
		int fact=1;
		if (nombre==0)
			return(1);
		else
		{
			for (int i=1;i<=nombre;i++)
				fact=fact*i;
			return fact;
		} 
	}  


	public double doubleFactorielle (int nombre) 
	{
		int fact=1;
		if (nombre==0)
			return(1);

		else
		{
			for (int i=1;i<=nombre;i++)
				fact=fact*i;
			return fact;
		}
	}  



	public BigInteger bigIntFactorielle (int nombre)
	{
		BigInteger fact= new BigInteger("1");
		for (int i=1; i<=nombre; i++)
			fact=fact.multiply(BigInteger.valueOf(i));
		return fact;

	}  

}