package org.e12107366.serie1.exo03;

public class Main {

	public static void main(String[] args) {

		Palindrome chaine = new Palindrome() ;
		System.out.println(" ''Rion noir'' est un Palindrome: " +chaine.palindrome("Rion noir")) ;
		System.out.println(" ''Esope reste ici et se repose'' est un Palindrome: " +chaine.palindrome("Esope reste ici et se repose")) ;
		System.out.println(" ''Elu par cette crapule'' est un Palindrome: " +chaine.palindrome("Elu par cette crapule")) ;
		System.out.println(" ''Et la marine va venir a Malte'' est un Palindrome: " +chaine.palindrome("Et la marine va venir a Malte")) ;
		System.out.println(" ''Severe, dissuasive, je vis aussi de reves'' est un Palindrome: " +chaine.palindrome("Severe, dissuasive, je vis aussi de reves")) ;

	}

}