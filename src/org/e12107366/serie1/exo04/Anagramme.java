package org.e12107366.serie1.exo04;
import java.util.Arrays;

public class Anagramme {
	public boolean isAnagramme(String s1, String s2) {
		String ch1=s1.toLowerCase();
		String ch2=s2.toLowerCase();
		char[] tab1=ch1.toCharArray();
		char[] tab2=ch2.toCharArray();
		Arrays.sort(tab1);
		Arrays.sort(tab2);
		return Arrays.equals(tab1,tab2);

	}

}