package org.e12107366.serie1.exo04;

public class Main {

	public static void main(String[] args) {
		
		Anagramme anagramme = new Anagramme() ;
		
		System.out.println("Paul et Ngolo sont  anagrammes : " +anagramme.isAnagramme("Paul", "Ngolo")) ;
		System.out.println("Telecom et Galilee sont  anagrammes : " +anagramme.isAnagramme("Telecom", "Galilee")) ;
		System.out.println("Bonjour le monde et Hello world sont  anagrammes : "  +anagramme.isAnagramme("Bonjour le monde", "Hello world")) ;
		
		
		System.out.println("Galilee et egaille sont  anagrammes : " +anagramme.isAnagramme("Galilee "," egaille")) ;
		System.out.println("Serveur et reveurs sont  anagrammes : " +anagramme.isAnagramme("Serveur "," reveurs")) ;
		System.out.println("Boris Vian et Bison ravi sont  anagrammes : "  +anagramme.isAnagramme("Boris Vian","Bison ravi")) ;
		System.out.println("La foret amazonienne et La zone enorme fanait sont  anagrammes : " +anagramme.isAnagramme("La foret amazonienne ","La zone enorme fanait")) ;
	}

}