package org.e12107366.serie2.exo05;
import java.util.Objects;
public class User {


	private String nom;
	private String prenom;
	private int salaire;

	public User (String nom, String prenom, int salaire)//constructeur
	{
		this.nom=nom;
		this.prenom=prenom;
		this.salaire=salaire;
	}
	public User ()//constructeur
	{	

	}

	public User (String nom, int salaire)//constructeur
	{	
		new User(nom,"",salaire);
	}


	// les getter 
	String getterNom()
	{
		return this.nom;
	}
	String getterPrenom()
	{
		return this.prenom;
	}
	int getterSalaire()
	{
		return this.salaire;
	}

	// les setter 
	void setterNom(String nom) {
		this.nom=nom;
	}
	void setterPrenom(String prenom) {
		this.prenom=prenom;
	}
	void setterSalaire(int salaire) {
		this.salaire=salaire;
	}


	void raise (int raise)
	{
		this.salaire+=raise;
	}


	@Override
	public String toString() {
		return "user Prénom=" + prenom + ", Nom=" + nom + ", salaire=" + salaire ;
	}


	@Override
	public int hashCode() {
		return Objects.hash(nom, prenom);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return this.nom.equals(other.getterNom()) && this.prenom.equals(other.getterPrenom());
		//return Objects.equals(nom, other.nom) && Objects.equals(prenom, other.prenom);

	}

	/*public int compareTO(Object obj) {
		String A=this.getterNom();
		if(nom.compareTo(A)==0)
		{
			String B=this.getterPrenom();
			return nom.compareTo(B);
		}
		else 
			return nom.compareTo(A);

	}*/
	public int compareTo(User o) {
		if (this.nom.equals(o.getterNom()))


			return this.prenom.compareTo(o.getterPrenom());
		else
			return this.nom.compareTo(o.getterNom());
	}









}




