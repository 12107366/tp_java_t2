package org.e12107366.serie2.exo06;

public class Exo6 {

	public static void main(String[] args) {

		User users[]=new User [5];

		users[0]=new User("Slimane","Azem",1918);
		users[1]=new User("elHachemi","Guerouabi",5588);
		users[2]=new User("Kamel","Messaoudi",1510);
		users[3]=new User("Abdelkader","Chaou",4500);
		users[4]=new User("dahmane","ElHarrachi",2084);


		System.out.println("le max est   :"+UserUtil.getMaxSalary(users));
		UserUtil.raise(users, 10);

		System.out.println("le max est     :"+UserUtil.getMaxSalary(users));
		System.out.println("la moyenne est :"+UserUtil.getAverageSalary(users));
		System.out.println("la médiane est :"+UserUtil.getMedianSalary(users));

	}

}
