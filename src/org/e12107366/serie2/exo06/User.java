package org.e12107366.serie2.exo06;
import java.util.Objects;


public class User implements Comparable <User>{
	private String nom;
	private String prenom;
	int salaire;

	public User (String nom, String prenom, int salaire)
	{
		this.nom=nom;
		this.prenom=prenom;
		this.salaire=salaire;
	}
	
	public User ()//
	{	
	}

	public User (String nom, int salaire)
	{	
		new User(nom,"",salaire);
	}


	public String getterNom()
	{
		return this.nom;
	}
	
	public String getterPrenom()
	{
		return this.prenom;
	}
	
	public int getterSalaire()
	{
		return this.salaire;
	}

	public void setterNom(String nom) {
		this.nom=nom;
	}
	
	public void setterPrenom(String prenom) {
		this.prenom=prenom;
	}
	
	public void setterSalaire(int salaire) {
		this.salaire=salaire;
	}

	public  void raise(int montant) {
		this.salaire += montant;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nom, prenom, salaire);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(nom, other.nom) && Objects.equals(prenom, other.prenom) && salaire == other.salaire;
	}

	public int compareTo(User usr) {
		if (this.nom.equals(usr.getterNom()))
			return this.prenom.compareTo(usr.getterPrenom());
		else
			return this.nom.compareTo(usr.getterNom());
	}
	
	@Override
	public String toString() {
		return "User [nom=" + nom + ", prenom=" + prenom + ", salaire=" + salaire + "]";
	}

}




