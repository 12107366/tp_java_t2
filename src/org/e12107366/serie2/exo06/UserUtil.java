package org.e12107366.serie2.exo06;
import java.util.Arrays; 
public class UserUtil {

	public static void raise(User[] users, int percentage)
	{
		int i;
		for( i=0;i<users.length;i++)
		{
			int S=users[i].getterSalaire();
			users[i].setterSalaire(S+ S * percentage) ;
		}
	}


	public static int  getMaxSalary(User[] users)
	{	
		int salaireMax=users[0].getterSalaire();
		for(int i=1;i<users.length;i++)
		{
			if(salaireMax<users[i].getterSalaire())
			{
				salaireMax=users[i].getterSalaire();
			}
		}

		return salaireMax;
	}


	public static int getAverageSalary(User[] users)
	{
		int salaires=0;
		for(int i=0;i<users.length;i++)
		{
			salaires=users[i].getterSalaire()+salaires;
		}
		return salaires/(users.length);
	}


	public static int getMedianSalary(User[] users)
	{
		int salaires[] = new int[users.length];
		for(int i=0;i<users.length;i++)
			salaires[i]=users[i].getterSalaire();
		Arrays.sort(salaires);
		if(users.length%2 == 0)

			return (salaires[(users.length-1)/2] + salaires[users.length/2])/2;

		else
			return salaires[users.length/2];
	}

}