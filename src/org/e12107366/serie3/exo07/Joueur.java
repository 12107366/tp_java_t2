package org.e12107366.serie3.exo07;
import java.util.ArrayList;
import java.util.List;

public class Joueur {
	public String nom;
	public int age;

	public Joueur(String nom, int age) {
		this.nom = nom;
		this.age = age;
	}

	String getterNom()
	{
		return this.nom;
	}

	int getterAge()
	{
		return this.age;
	}

	//  setter 
	void setterNom(String nom) {
		this.nom=nom;
	}

	void setterAge(int age) {
		this.age=age;

	}

	@Override
	public String toString() {
		return "Joueur nom=" + nom + ", age=" + age + "";
	}


}
