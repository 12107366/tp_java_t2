package org.e12107366.serie3.exo07;

import java.util.ArrayList;

public class EquipeLimitee {
	int nbrmax;
	ArrayList<Joueur> joueurs;

	EquipeLimitee (int a){
		this.nbrmax=a;
		this.joueurs=new ArrayList();
	}


	void addJoueur(Joueur a) {

		if(nbrmax>this.getNombreJoueurs()) {
			this.joueurs.add(a);
			System.out.println("\n joueur ajouté avec succès ");
		}
		else { System.out.println("\n ERREUR: le nombre de joueurs max est atteint");}
	}

	boolean removeJoueur(Joueur a)
	{
		return this.joueurs.remove(a);
	}

	void isJoueurPresent(Joueur j) {
		System.out.println(joueurs.contains(j)) ;
	}

	public String toString() {
		//
		StringBuilder str= new StringBuilder();
		str.append("Equipe: "+joueurs.size()+" joueurs\n");
		for(Joueur j:joueurs)
		{	
			str.append(j.toString()+"\n");
		}
		return str.toString();
	}


	public void addAllEquipe(Equipe e) {
		if((nbrmax-this.getNombreJoueurs())>e.getNombreJoueurs()){
			for (Joueur j : e.joueurs) {

				if (!joueurs.contains(j)) {
					this.addJoueur(j);
				}
			}System.out.println("\n l'equipe a été copiée avec succès \n");
		}
		else {System.out.println("\n ERREUR: nombre de joueurs trop important \n");}
	}


	public void clear() {	
		this.clear();
	}

	int getNombreJoueurs() {

		return joueurs.size();

	}


	public double getMoyenneAge(Equipe e)
	{
		double moy= 0; 
		for(Joueur j : e.joueurs )
		{
			moy=moy+(j.getterAge());  
		}
		return moy/e.getNombreJoueurs() ; 
	}


}
