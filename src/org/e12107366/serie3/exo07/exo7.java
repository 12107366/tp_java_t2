package org.e12107366.serie3.exo07;
import java.util.ArrayList;
import java.util.List;


public class exo7  {

	public static void main(String[] args) {
		int nbrmax=3;
		Equipe e=new Equipe();
		Joueur j1 = new Joueur("Fabien", 26);
		Joueur j2 = new Joueur("Lilian", 26);
		Joueur j3 = new Joueur("Bixente", 28);
		Joueur j4 = new Joueur("Youri", 30);

		e.addJoueur(j1);
		e.addJoueur(j2);
		e.addJoueur(j2);
		System.out.println("après avoir ajouté j1 et j2 :\n "+e);
		e.removeJoueur(j1);
		System.out.println("après avoir suprimé j1 \n "+e);
		e.clear();
		System.out.println("après avoir clear l'equipe \n"+e);
		e.addJoueur(j3);
		e.addJoueur(j4);
		System.out.println("après avoir ajouté j3 et j4 on verifie leur presence et la non présence de J1 \n ");
		e.isJoueurPresent(j3);
		e.isJoueurPresent(j4);
		e.isJoueurPresent(j1);

		System.out.println("le nombre de Joueurs de la liste est "+e.getNombreJoueurs());
		System.out.println(e.getMoyenneAge(e));
		e.addJoueur(j2);
		System.out.println("le nombre de Joueurs de la liste est "+e.getNombreJoueurs());


		// EQUIPE LIMITEE

		EquipeLimitee equipe1 = new EquipeLimitee(nbrmax); 

		equipe1.addJoueur(j1);
		equipe1.addJoueur(j2);
		equipe1.addJoueur(j3);
		equipe1.addJoueur(j4);

		equipe1.joueurs.clear();
		equipe1.addAllEquipe(e);

		e.removeJoueur(j2);
		equipe1.addAllEquipe(e);

	}








}
