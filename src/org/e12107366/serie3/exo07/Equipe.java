package org.e12107366.serie3.exo07;
import java.util.ArrayList;
import java.util.List;


public class Equipe {
	ArrayList<Joueur> joueurs = new ArrayList<>();

	void addJoueur(Joueur j) {
		this.joueurs.add(j);
	}

	boolean removeJoueur(Joueur j)
	{
		return this.joueurs.remove(j);
	}

	void isJoueurPresent(Joueur j) {
		System.out.println(this.joueurs.contains(j)) ;
	}

	public String toString() {
		//
		StringBuilder str= new StringBuilder();
		str.append("Equipe: "+joueurs.size()+" joueurs\n");
		for(Joueur j:joueurs)
		{	
			str.append(j.toString()+"\n");
		}
		return str.toString();
	}


	public void addAllEquipe(Equipe e) {
		for (Joueur j : e.joueurs) {

			if (!joueurs.contains(j)) {
				this.addJoueur(j);
			}
		}
	}


	public void clear() {	
		this.joueurs.clear();
	}

	int getNombreJoueurs() {

		return joueurs.size();

	}


	public double getMoyenneAge(Equipe e)
	{
		double moy= 0; 
		for(Joueur j : e.joueurs )
		{
			moy=moy+(j.getterAge());  
		}
		return moy/e.getNombreJoueurs() ; 
	}






}
