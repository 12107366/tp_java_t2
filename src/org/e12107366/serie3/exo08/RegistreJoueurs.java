package org.e12107366.serie3.exo08;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
public class RegistreJoueurs {

	private HashMap<Integer, ArrayList<Joueur>> Hmap = new HashMap();

	public void addJoueur(Joueur j) {
		int decennie=j.getAnneeDeNaissance();
		decennie=(decennie/10)*10;
		if(!Hmap.containsKey(decennie)) {
			ArrayList<Joueur> liste= new ArrayList();
			Hmap.put(decennie, liste);	
			Hmap.get(decennie).add(j);
		}
		else {
			Hmap.get(decennie).add(j);
		}

	}
	public ArrayList<Joueur> get(int decennie){
		if(Hmap.containsKey(decennie)) {
			return Hmap.get(decennie);
		}
		else return null;
	}

	public int count(int decennie) {

		return Hmap.get(decennie).size();
	}

	public int count() {
		int somme = 0;
		Set<Integer> keys = Hmap.keySet();
		for(Integer key: keys) {
			somme =somme+ Hmap.get(key).size();	
		}
		return somme;
	}

	public String toString() {
		StringBuilder string = new StringBuilder();
		Set<Integer> keys = Hmap.keySet();
		for(Integer decennie: keys) {
			string.append(decennie+" :\t"+Hmap.get(decennie)+ "\n");
		}
		return string.toString();
	}


}