package org.e12107366.serie3.exo08;

public class Joueur {
	private String nom;
	private int anneeDeNaissance;

	public Joueur() {

	}

	public Joueur(String nom,int anneeDeNaissance)
	{
		this.nom=nom;
		this.anneeDeNaissance=anneeDeNaissance;

	}

	public String getNom()
	{
		return nom;
	}

	public void setNom(String nom)
	{
		this.nom = nom;
	}


	public int getAnneeDeNaissance()
	{
		return anneeDeNaissance;
	}

	public void setAnneeDeNaissance(int anneeDeNaissance) 
	{
		this.anneeDeNaissance = anneeDeNaissance;
	}
	
	@Override
	public String toString() {
		return "\nJoueur  nom=" + nom + "\tannée de naissance=" + anneeDeNaissance;
	}

}



