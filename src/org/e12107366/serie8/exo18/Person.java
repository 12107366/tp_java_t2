package org.e12107366.serie8.exo18;
import java.util.function.Function;

public class Person {
        @Override
	public String toString() {
		return "Person [Nom=\t" + Nom + ",\tPrenom=\t" + Prenom + ",\tage=\t" + age + "]";
	}



		private String Nom;
        private String Prenom;
        private int age;
    
        public Person(String Nom, String Prenom, int age) {
            this.Nom = Nom;
            this.Prenom = Prenom;
            this.age = age;
        }
    
        public String getNom() {
            return Nom;
        }
    
        public void setNom(String Nom) {
            this.Nom = Nom;
        }
    
        public String getPrenom() {
            return Prenom;
        }
    
        public void setPrenom(String Prenom) {
            this.Prenom = Prenom;
        }
    
        public int getAge() {
            return age;
        }
    
        public void setAge(int age) {
            this.age = age;
        }
        
        
        
        Function<String, Person> lineToPerson = line -> {
            if (line.startsWith("#")) {
                return null;
            }
            String[] parts = line.split(",");
            if (parts.length != 3) {
                return null;
            }
            
            String lastName = parts[0].trim();
            String firstName = parts[1].trim();
            int age = Integer.parseInt(parts[2].trim());
            return new Person(lastName, firstName, age);
        };
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }