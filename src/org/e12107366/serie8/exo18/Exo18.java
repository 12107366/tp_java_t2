package org.e12107366.serie8.exo18;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Exo18 {

	public static void main(String[] args) throws IOException {

		List<Person> personnes = new ArrayList<>();
		personnes.add(new Person("Chaou", "Abdelkader", 81));
		personnes.add(new Person("Messa", "Kamel", 37 ));
		personnes.add(new Person("Azem", "Slimane", 64));

		PersonWriter writer = new PersonWriter();
		writer.write(personnes, "Singer.txt");


		PersonReader reader = new PersonReader();
		List<Person> personnes1 = reader.read("Singer.txt");
		for (Person person : personnes1) {
			System.out.println(person);
		}
	}
}