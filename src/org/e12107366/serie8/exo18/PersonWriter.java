package org.e12107366.serie8.exo18;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;


public class PersonWriter {
	public void write(List<Person> personnes, String fileName) {
		try (PrintWriter writer = new PrintWriter(new File(fileName))) {
			for (Person person : personnes) {
				writer.println(formatPerson(person));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private String formatPerson(Person person) {
		return person.getNom() + ", " + person.getPrenom() + ", " + person.getAge();
	}
