package org.e12107366.serie8.exo18;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonReader {
    public List<Person> read(String fileName) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            List<Person> people = br.lines().flatMap(line -> {
                        if (line.startsWith("#")) {
                            return Stream.empty();
                        }
                        
                        return Stream.of(lineToPerson.apply(line));
                    })
                    .filter(person -> person != null)
                    .collect(Collectors.toList());
            return people;
        }
    }
    
    // Function to convert a line of text to a Person object
    private Function<String, Person> lineToPerson = line -> {
        if (line.startsWith("#")) {
            return null;
        }
        String[] parts = line.split(",");
        if (parts.length != 3) {
            return null;
        }
        String lastName = parts[0].trim();
        String firstName = parts[1].trim();
        int age = Integer.parseInt(parts[2].trim());
        return new Person(lastName, firstName, age);
    };
}