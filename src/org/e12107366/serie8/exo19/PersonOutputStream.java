package org.e12107366.serie8.exo19;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class PersonOutputStream extends FileOutputStream {

    public PersonOutputStream(File file) throws FileNotFoundException  {
        super(file);
    }
    
    public void writeFields(List<Person> personnes) throws IOException {
        try (DataOutputStream dataOut = new DataOutputStream(this)) {
            dataOut.writeInt(personnes.size());
            for (Person person : personnes) {
                byte[] personBytes = person.toByteArray();
                dataOut.writeInt(personBytes.length);
                dataOut.write(personBytes);
            }
        }
    }

    @Override
    public void close() throws IOException {
        super.close();
    }

}

 

