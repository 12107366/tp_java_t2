package org.e12107366.serie8.exo19;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        
        List<Person> personnes = new ArrayList<>();
        personnes.add(new Person("chaou", "Abdelkader", 81));
        personnes.add(new Person("Messa", "Kamel", 37));
        personnes.add(new Person("Azem", "Slimane", 64));

       
        
        
        try (PersonOutputStream out = new PersonOutputStream(new File("Singer.txt"))) {
            out.writeFields(personnes);
        }

    
        List<Person> readpersonnes;
        try (PersonInputStream in = new PersonInputStream(new File("personnes.txt"))) {
            readpersonnes = in.readFields();
        }

        
        for (Person person : readpersonnes) {
            System.out.println(person.getNom() + " " + person.getPrenom() + ", " + person.getAge() + " ans");
        }
    }
}

