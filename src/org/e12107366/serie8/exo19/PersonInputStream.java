package org.e12107366.serie8.exo19;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;

public class PersonInputStream extends FileInputStream {

    public PersonInputStream(File file) throws FileNotFoundException {
        super(file);
    }
    
    private DataInputStream in;

    public List<Person> readFields() throws IOException {
        List<Person> personnes = new ArrayList<>();
        int numpersonnes = in.readInt();
        for (int i = 0; i < numpersonnes; i++) {
            int NomLength = in.readInt();
            byte[] NomBytes = new byte[NomLength];
            in.readFully(NomBytes);
            String Nom = new String(NomBytes, "UTF-8");

            int PrenomLength = in.readInt();
            byte[] PrenomBytes = new byte[PrenomLength];
            in.readFully(PrenomBytes);
            String Prenom = new String(PrenomBytes, "UTF-8");

            int age = in.readInt();
            personnes.add(new Person(Nom, Prenom, age));
        }
        return personnes;
    }

    public void close() throws IOException {
        in.close();
    }
}

