package org.e12107366.serie8.exo19;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.function.Function;

public class Person implements Serializable {
    private static final long serialVersionUID = 1L;
        @Override
	public String toString() {
		return "Person [Nom=\t" + Nom + ",\tPrenom=\t" + Prenom + ",\tage=\t" + age + "]";
	}

		private String Nom;
        private String Prenom;
        private int age;
    
        public Person(String Nom, String Prenom, int age) {
            this.Nom = Nom;
            this.Prenom = Prenom;
            this.age = age;
        }
    
        public String getNom() {
            return Nom;
        }
    
        public void setNom(String Nom) {
            this.Nom = Nom;
        }
    
        public String getPrenom() {
            return Prenom;
        }
    
        public void setPrenom(String Prenom) {
            this.Prenom = Prenom;
        }
    
        public int getAge() {
            return age;
        }
    
        public void setAge(int age) {
            this.age = age;
        }
        
        
        
        Function<String, Person> lineToPerson = line -> {
            if (line.startsWith("#")) {
                return null;
            }
            String[] parts = line.split(",");
            if (parts.length != 3) {
                return null;
            }
            
            String Nom = parts[0].trim();
            String Prenom = parts[1].trim();
            int age = Integer.parseInt(parts[2].trim());
            return new Person(Nom, Prenom, age);
        };   
        
        
        public byte[] toByteArray() throws IOException {
            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            try (DataOutputStream dataOut = new DataOutputStream(byteOut)) {
                dataOut.writeUTF(Nom);
                dataOut.writeUTF(Prenom);
                dataOut.writeInt(age);
            }
            return byteOut.toByteArray();
        }
        
   
    }

