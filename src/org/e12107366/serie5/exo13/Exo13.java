package org.e12107366.serie5.exo13;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
public class Exo13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Integer, String> map = new HashMap<>();
		map.put(1,"one");
		map.put(2,"two");
		map.put(3,"three");
		map.put(4,"four");
		map.put(5,"five");
		map.put(6,"six");
		map.put(7,"seven");
		map.put(8,"eight");
		map.put(9,"nine");
		map.put(10,"ten");
		map.put(11,"eleven");
		map.put(12,"twelve");
		//QST1
		for (String value : map.values()) {
			value = value.toUpperCase();
		}
		
		List<Integer> keys = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);    

		//QST2
		for (int key : keys)
			map.putIfAbsent(key, "");

		//QST 3 o  peut utliser la méthode getOrDefault 
		for (Integer key : keys) {
			String value = map.getOrDefault(key, "").toUpperCase();
			System.out.println(key+" : "+value);
		}

		//qst 4
		ArrayList<String> words = new ArrayList<>();
		words.add("one");
		words.add("two");
		words.add("three");
		words.add("four");
		words.add("five");
		words.add("six");
		words.add("seven");
		words.add("eight");
		words.add("nine");
		words.add("ten");
		words.add("eleven");
		words.add("twelve");


		Map<Integer, List<String>> hashTable = new HashMap<>();
		
		for (String word : words)
		{
			int key = word.length();
			if (hashTable.containsKey(key))
			{
				hashTable.get(key).add(word);
			}
			else 
			{
				List<String> list = new ArrayList<>();
				list.add(word);
				hashTable.put(key, list);
			}
		}


	}
}
