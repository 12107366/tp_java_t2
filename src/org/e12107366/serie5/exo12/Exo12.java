package org.e12107366.serie5.exo12;
import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.ArrayList;
import java.util.Comparator;
public class Exo12 {

	public static void main(String[] args) {
		ArrayList<String> words = new ArrayList<>();
		words.add("one");
		words.add("two");
		words.add("three");
		words.add("four");
		words.add("five");
		words.add("six");
		words.add("seven");
		words.add("eight");
		words.add("nine");
		words.add("ten");
		words.add("eleven");
		words.add("twelve");
		
		words.forEach(System.out::println);
		words.removeIf(n->n.length()%2==0);
		System.out.println("\n supprimer si la taille du mot est paire  \n");
		words.forEach(System.out::println);
		words.replaceAll(n->n.toUpperCase());
		System.out.println("\n  tout en majuscule  \n");
		words.forEach(System.out::println);
		words.replaceAll(n->n.toLowerCase()); // pour voir le reste des qst
		Predicate<String> commenceParVoy = str ->
		{
			String firstL = str.substring(0, 1).toLowerCase();
			return "aeiouy".contains(firstL);
		};

		//condition ? si vrai : si faux
		System.out.println("\n mettre les mots qui commencent par une voyelle en majuscule  \n");
		words.replaceAll(n->commenceParVoy.test(n)?n.toUpperCase():n);
		words.forEach(System.out::println);

		System.out.println("\n trier  le tableau par la taille \n");
		Comparator<String>compL=Comparator.comparingInt(String::length);
		words.sort(compL);
		words.forEach(System.out::println);

		System.out.println("\n trier le tableau par la taille ensuite par l'ordre  \n");
		Comparator<String>comp2=Comparator.comparingInt(String::length).thenComparing(Comparator.naturalOrder());
		words.sort(comp2);
		words.forEach(System.out::println);
		
	}


}
