package org.e12107366.serie5.exo14;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class Exo14 {
	public static void main(String[] args) {
	List<String> maList = new ArrayList<>();
    maList.add("one");
    maList.add("two");
    maList.add("three");
    maList.add("four");
    maList.add("five");
    maList.add("six");
    maList.add("seven");
    maList.add("eight");
    maList.add("nine");
    maList.add("ten");
    maList.add("eleven");
    maList.add("twelve");
    
    Stream<String> monStream = maList.stream();
    //afficher le stream
    monStream.forEach(System.out::println);

    Stream<String> majsculeStream = maList.stream().map(String::toUpperCase);
    majsculeStream.forEach(System.out::println);
    
    Stream<String> firstLetterStream = maList.stream().map(s -> s.substring(0, 1).toUpperCase()).distinct();
    firstLetterStream.forEach(System.out::println);
    
    Stream<Integer> TailleStream = maList.stream().map(String::length).distinct();
    TailleStream.forEach(System.out::println);
    
    long count = maList.stream().count();
    System.out.println(count);
    
    long count2 = maList.stream().filter(s->s.length()%2==0).count();
    System.out.println(count);
    
    List <String> Impair = maList.stream().map(mot ->mot.toUpperCase()).
			filter(taille -> taille.length()%2!=0).collect(Collectors.toList()); 
    Impair.forEach(System.out::println);
}
}