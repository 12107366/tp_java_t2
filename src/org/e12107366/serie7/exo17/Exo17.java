package org.e12107366.serie7.exo17;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Exo17 {
	public static void main(String[] args) {
	    Map<Integer, Integer> histogram=buildHistogram();
	    System.out.println("Histogramme des mots de 2 lettres et plus en fonction de leur longueur : \n");
	    System.out.println("\nType de la table de cette table de hachage : "+histogram.getClass());
	    System.out.println("\nNombre de paires clés / valeurs : "+histogram.size());
	    System.out.println("\nPaires clés / valeurs :"+histogram);
	    int plusFreq=longueurPlusFreq(histogram);
	    System.out.println("Longueur de mots la plus fréquente:" + plusFreq);

	    int cptplusFreq=histogram.get(plusFreq);
	    System.out.println("Nombre de mots ayant cette longueur:" + cptplusFreq);
	    
	    int medianLength=TailleMediane(histogram);
	    System.out.println("Longueur de mot médiane de l'histogramme:" + medianLength);
	    
	    int cptMedianLength=histogram.get(medianLength);
	    System.out.println("Nombre de mots ayant cette longueur : " +cptMedianLength);
	}

	public static Map<Integer,Integer> buildHistogram(){
	    List<String> germinalLines= GerminalStream.getGerminalLines();
	    Map<Integer,Integer> histogram= new HashMap<>();
	    
	    for (String line:germinalLines){
	        String[] words=line.split("[^\\p{L}]+");
	        for (String word:words){
	            if (word.length()>=2) {
	                int length = word.length();
	                histogram.put(length,histogram.getOrDefault(length,0)+1);
	            }
	        }
	    }
	    return histogram;
	}

	public static int longueurPlusFreq(Map<Integer, Integer> histogram) {
	    int plusFreq = 0;
	    int maxcpt = 0;
	    for (Map.Entry<Integer, Integer> entry:histogram.entrySet()) {
	        int length=entry.getKey();
	        int cpt=entry.getValue();
	        if (cpt>maxcpt) {
	            plusFreq=length;
	            maxcpt=cpt;
	        }
	    }
	    return plusFreq;
	}

	public static int TailleMediane(Map<Integer, Integer> histogram) {
	    int mots = histogram.values().stream().mapToInt(Integer::intValue).sum();
	    int med=mots/2;
	    int motsCpt=0;
	    for (Map.Entry<Integer, Integer> entry:histogram.entrySet()) {
	        int length=entry.getKey();
	        int cpt=entry.getValue();
	        motsCpt+=cpt;
	        if (motsCpt>=med){
	            return length;
	        }
	    }
	    return -7;
	}

}