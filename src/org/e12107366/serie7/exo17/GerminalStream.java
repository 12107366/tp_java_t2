package org.e12107366.serie7.exo17;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GerminalStream {
	public static List<String> readLinesFrom(String fileName) {
		Path path = Paths.get(fileName);
		try (Stream<String> lines = Files.lines(path)) {
			return lines.collect(Collectors.toList());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public static List<String> getGerminalLines() {
		List<String> allLines=readLinesFrom("germinal.txt");
		List<String> germinalLines=allLines.subList(70,allLines.size()-322);
		return germinalLines;
	}

	public static long countNonEmptyLines(List<String> lines) {
		return lines.stream().filter(line->!line.trim().isEmpty()).count();
	}

	public static long countBonjour(List<String> lines) {
		return lines.stream().map(line->line.toLowerCase()).filter(line->
		line.contains("bonjour")).count();
	}

	public static Stream<Character> getCharactersFromLine(String line) {
		return line.chars().mapToObj(c->(char)c);
	}

	public static Stream<Character> getCharactersFromGerminalLines() {
		return getGerminalLines().stream().flatMap(line->getCharactersFromLine(line));
	}


}