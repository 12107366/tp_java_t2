package org.e12107366.serie7.exo16;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.BiFunction;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.*;
import java.util.stream.*;
import java.nio.file.*;
import java.io.*;

public class Exo16 {

	public static List<String> readLinesFrom(String fileName) {
		Path path = Paths.get(fileName);
		try (Stream<String> lines = Files.lines(path)) {
			return lines.collect(Collectors.toList());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	

	public static List<String> GetLignes() {
		List<String> allLines = readLinesFrom("germinal.txt");
		List<String> germinalLines = allLines.subList(70, allLines.size() - 322);
		return germinalLines;
	}

	public static long LignesNonVides(List<String> ligne) {
		return ligne.stream().filter(line->!line.trim().isEmpty()).count();
	}

	
	public static long nbrBonjour(List<String> ligne) {
		return ligne.stream().map(line->line.toLowerCase()).filter(line -> 
		ligne.contains("bonjour")).count();
	}

	public static Stream<Character> getCharLigne(String ligne) {
		return ligne.chars().mapToObj(c->(char)c);}

	
	public static Stream<Character> getCharLigneGerminal() {
		return GetLignes().stream().flatMap(line -> getCharLigne(line));
	}

	
	public static void main(String[] args) {
		
		
		List<String> germinalLines = GetLignes();
		System.out.println("il y a " + germinalLines.size() + " lignes dans le texte ");
		
		long cmptLigneNonVide = LignesNonVides(germinalLines);
		System.out.println("il y a " + cmptLigneNonVide + " lignes non vides");

		long cmptBonjour = nbrBonjour(germinalLines);
		System.out.println("bonjour est écrot " + cmptBonjour + "fois");

		Stream<Character> germinalChar = getCharLigneGerminal();
		germinalChar.forEach(System.out::print);

		

		// Question 7
		BiFunction<String, String, Stream<String>> splitWordWithPattern =
			(line, pattern) -> Pattern.compile("[" + pattern + "]").splitAsStream(line);
		Stream<String> Mots = GetLignes().stream()
			.flatMap(line -> splitWordWithPattern.apply(line, " !\\-"));
		long cmptNbrMot = Mots.count();
		System.out.println("\n\nLe roman Germinal contient " + cmptNbrMot + " mots au total.");


		// Question 8
		OptionalInt tailleplusLong = Mots.mapToInt(String::length).max();
		System.out.println("Le mot le plus long de Germinal a " + tailleplusLong.getAsInt() + " lettres.");
		
	}

}















